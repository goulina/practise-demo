import 'package:flutter/material.dart';
import 'package:practice/model/people_entity.dart';
import 'package:practice/model/pet_entity.dart';
import 'package:provider/provider.dart';

class ProviderPage extends StatelessWidget {
  const ProviderPage({super.key});

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: const Text("ProviderPage",style: TextStyle(color: Colors.white),),
         backgroundColor: Theme.of(context).colorScheme.inversePrimary,
       ),
       body: Center(
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           children: [
             ElevatedButton(
                 child: const Text("小白改名"),
                 onPressed: () {
                   String name ='${Provider.of<PeopleEntity?>(context,listen: false)?.pets[0].name}1';
                   Provider.of<PeopleEntity?>(context,listen: false)?.firstPetRename(name);
                 }
             ),
             ElevatedButton(
                 child: const Text("小黑改名"),
                 onPressed: () {
                   String name ='${Provider.of<PeopleEntity?>(context,listen: false)?.pets[1].name}1';
                   Provider.of<PeopleEntity?>(context,listen: false)?.secondPetRename(name);
                 }
             ),

             Selector<PeopleEntity,String>(
               selector: (BuildContext , PeopleEntity ) {
                 return PeopleEntity.pets.first.name!;
               },

               builder: (BuildContext context, String name, Widget? child) {
                 print("小白");
                 return Text("The pet's name is $name");
               },
             )
           ],
         ),
       )
     );
  }

  // Widget buildText() {
  //   return Builder(builder: (BuildContext context){
  //     print("0000");
  //     return Text(context.watch<PeopleEntity>().pets[0].name!);
  //   });
  // }

}