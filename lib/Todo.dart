class Todo {
  int id;
  String name;
  bool done;

  Todo(this.id, this.name, this.done);
}