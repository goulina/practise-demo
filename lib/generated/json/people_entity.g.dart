import 'package:practice/generated/json/base/json_convert_content.dart';
import 'package:practice/model/people_entity.dart';
import 'package:flutter/foundation.dart';

import 'package:practice/model/pet_entity.dart';


PeopleEntity $PeopleEntityFromJson(Map<String, dynamic> json) {
  final PeopleEntity peopleEntity = PeopleEntity();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    peopleEntity.name = name;
  }
  final List<PetEntity>? pets = (json['pets'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<PetEntity>(e) as PetEntity).toList();
  if (pets != null) {
    peopleEntity.pets = pets;
  }
  return peopleEntity;
}

Map<String, dynamic> $PeopleEntityToJson(PeopleEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['pets'] = entity.pets.map((v) => v.toJson()).toList();
  return data;
}

extension PeopleEntityExtension on PeopleEntity {
  PeopleEntity copyWith({
    String? name,
    List<PetEntity>? pets,
  }) {
    return PeopleEntity()
      ..name = name ?? this.name
      ..pets = pets ?? this.pets;
  }
}