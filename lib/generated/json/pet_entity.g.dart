import 'package:practice/generated/json/base/json_convert_content.dart';
import 'package:practice/model/pet_entity.dart';

PetEntity $PetEntityFromJson(Map<String, dynamic> json) {
  final PetEntity petEntity = PetEntity();
  final String? name = jsonConvert.convert<String>(json['name']);
  if (name != null) {
    petEntity.name = name;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    petEntity.type = type;
  }
  return petEntity;
}

Map<String, dynamic> $PetEntityToJson(PetEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['name'] = entity.name;
  data['type'] = entity.type;
  return data;
}

extension PetEntityExtension on PetEntity {
  PetEntity copyWith({
    String? name,
    String? type,
  }) {
    return PetEntity()
      ..name = name ?? this.name
      ..type = type ?? this.type;
  }
}