import 'dart:io';

import 'package:flutter/material.dart';
import 'package:practice/Todo.dart';

import 'OtherWidget.dart';

class ShareDataWidget extends InheritedWidget {
  final int data;

  const ShareDataWidget( { required this.data, super.key, required super.child});

  static ShareDataWidget? of(BuildContext context) {
    // return context.dependOnInheritedWidgetOfExactType<ShareDataWidget>();
    return context.getElementForInheritedWidgetOfExactType<ShareDataWidget>()!.widget as ShareDataWidget;
  }


  @override
  bool updateShouldNotify(ShareDataWidget oldWidget) {
    return oldWidget.data != data;
  }

}

class TestWidget extends StatefulWidget {

  const TestWidget({super.key});

  @override
  TestWidgetState createState() => TestWidgetState();
  
}

class TestWidgetState extends State<TestWidget> {
  int? count ;

  @override
  Widget build(BuildContext context) {
    print("testBuild");

    count = ShareDataWidget.of(context)!.data;

    return Column(
      children: [
        Text("count:  $count",style: const TextStyle(fontSize: 30),),
        OtherWidget()
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("Dependencies change");
  }

  @override
  void didUpdateWidget(TestWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
}

class InheritedWidgetPage extends StatefulWidget {
  InheritedWidgetPage( {super.key});

  @override
  InheritedWidgetPageState createState() => InheritedWidgetPageState();
  
}

class InheritedWidgetPageState extends State<InheritedWidgetPage> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text("InheritedWidgetPage",style: TextStyle(color: Colors.white),),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                child: const Text("count++",style: TextStyle(fontSize: 24)),
                onPressed: () => setState(() => count++),
              ),
              ShareDataWidget(
                  data: count,
                  child: TestWidget()
              ),
            ],
          ),
        )
    );
  }

  @override
  void didUpdateWidget(InheritedWidgetPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('widgetA didUpdateWidget');
  }
}

