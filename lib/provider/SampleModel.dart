
import 'package:flutter/cupertino.dart';

class SampleModel extends ChangeNotifier {
  int count = 0;

  countIncrease() {
    count++;
    notifyListeners();
  }
}