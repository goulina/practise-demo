import 'package:flutter/material.dart';
import 'MyInheritedProvider.dart';

class MyChangeNotifierProvider<T extends ChangeNotifier> extends StatefulWidget {
  final Widget child;
  final T data;

  MyChangeNotifierProvider({
    Key? key,
    required this.data,
    required this.child,
  });

  static T? of<T> (BuildContext context,{bool listen = true}) {
    // final provider = context.getElementForInheritedWidgetOfExactType<MyInheritedProvider<T>>()!.widget as MyInheritedProvider<T>;
    final MyInheritedProvider<T>? provider =  listen ?
    context.dependOnInheritedWidgetOfExactType<MyInheritedProvider<T>>():
    context.getElementForInheritedWidgetOfExactType<MyInheritedProvider<T>>()!.widget as MyInheritedProvider<T>;
    return provider?.data;
  }

  @override
  _MyChangeNotifierProviderState<T> createState() =>
      _MyChangeNotifierProviderState<T>();
}

class _MyChangeNotifierProviderState<T extends ChangeNotifier>
    extends State<MyChangeNotifierProvider<T>> {

  void update() {
    setState(() => {});
  }

  @override
  Widget build(BuildContext context) {
    return MyInheritedProvider<T>(
      data: widget.data,
      child: widget.child,
    );
  }

  @override
  void initState() {
    widget.data.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    widget.data.removeListener(update);
    super.dispose();
  }

  @override
  void didUpdateWidget(MyChangeNotifierProvider<T> oldWidget) {
    if (widget.data != oldWidget.data) {
      oldWidget.data.removeListener(update);
      widget.data.addListener(update);
    }
    super.didUpdateWidget(oldWidget);
  }
}
