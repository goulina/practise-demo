import 'package:flutter/cupertino.dart';
import 'package:practice/provider/MyChangeNotifierProvider.dart';

class MyConsumer<T> extends StatelessWidget {

  MyConsumer({
    Key? key,
    required this.builder,
}) :super(key: key);

  final Widget Function(BuildContext context,T? value) builder;

  @override
  Widget build(BuildContext context) {
    return builder(
      context,
      MyChangeNotifierProvider.of<T>(context),
    );
  }
}