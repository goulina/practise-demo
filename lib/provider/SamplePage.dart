import 'package:flutter/material.dart';
import 'package:practice/provider/MyConsumer.dart';
import '../OtherWidget.dart';
import 'MyChangeNotifierProvider.dart';
import 'SampleModel.dart';

class SamplePage extends StatelessWidget {
  const SamplePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MyChangeNotifierProvider",style: TextStyle(color: Colors.white),),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: MyChangeNotifierProvider<SampleModel>(
        data: SampleModel(),
        child:_buildBody()
      ),
    );
  }

  _buildBody() {
    print("_buildBodyBuild");
    return Builder(builder: (context) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
                child: const Text("count++",style: TextStyle(fontSize: 24)),
                onPressed: () => {
                  MyChangeNotifierProvider.of<SampleModel>(context,listen: false)?.countIncrease()
                }),
            TestWidget(),
            OtherWidget()
          ],
        ),
      );
    });
  }
}

class TestWidget extends StatefulWidget {

  const TestWidget({super.key});

  @override
  TestWidgetState createState() => TestWidgetState();

}

class TestWidgetState extends State<TestWidget> {
  int? count ;

  @override
  Widget build(BuildContext context) {
    print("testBuild");


    return Column(
      children: [
        MyConsumer<SampleModel>(
          builder: (context,sampleModel) => Text("count: ${sampleModel?.count.toString()}",style: const TextStyle(fontSize: 30),)
        ),
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("Dependencies change");
  }

  @override
  void didUpdateWidget(TestWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }
}
