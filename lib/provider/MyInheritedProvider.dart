import 'package:flutter/material.dart';


class MyInheritedProvider<T> extends InheritedWidget {
  final T data;

  const MyInheritedProvider(
       {
         required this.data,
         required Widget child,
      }) : super(child: child);

  @override
  bool updateShouldNotify(MyInheritedProvider<T> old) {
    return true;
  }
}