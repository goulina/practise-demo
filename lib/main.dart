import 'package:flutter/material.dart';
import 'package:practice/Provider_demo.dart';
import 'package:practice/model/people_entity.dart';
import 'package:practice/provider/SamplePage.dart';
import 'package:provider/provider.dart';

import 'InheritedWidget_demo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  Map<String, WidgetBuilder>? routes;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => PeopleEntity(),
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.green),
            useMaterial3: true,
          ),
          routes: {
            "inheritedWidget_page": (context) => InheritedWidgetPage(),
            "myChangeNotifierProvider_page": (context) => SamplePage(),
            "provider_page": (context) => ProviderPage(),
          },
          home: MyHomePage(),
    ));
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed("inheritedWidget_page");
                },
                child: Text("InheritedWidget",style: TextStyle(fontSize: 24))
            ),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed("myChangeNotifierProvider_page");
                },
                child: Text("MyChangeNotifierProvider",style: TextStyle(fontSize: 24))
            ),
            TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed("provider_page");
                },
                child: Text("Provider",style: TextStyle(fontSize: 24))
            )
          ],
        ),
      )
    );
  }
}
