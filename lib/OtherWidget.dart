import 'package:flutter/material.dart';

class OtherWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("不依赖共享数据的其他组件 build");
    return Text("不依赖共享数据的其他组件",style: const TextStyle(fontSize: 30),);
  }

}