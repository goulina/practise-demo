import 'package:practice/generated/json/base/json_field.dart';
import 'package:practice/generated/json/pet_entity.g.dart';
import 'dart:convert';
export 'package:practice/generated/json/pet_entity.g.dart';

@JsonSerializable()
class PetEntity {
	String? name = '';
	String? type= '';

	PetEntity();

	PetEntity.init(this.name,this.type);

	factory PetEntity.fromJson(Map<String, dynamic> json) => $PetEntityFromJson(json);

	Map<String, dynamic> toJson() => $PetEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}