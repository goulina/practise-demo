
import 'package:flutter/foundation.dart';
import 'package:practice/generated/json/base/json_field.dart';
import 'package:practice/generated/json/people_entity.g.dart';
import 'dart:convert';

import 'package:practice/model/pet_entity.dart';
export 'package:practice/generated/json/people_entity.g.dart';

@JsonSerializable()
class PeopleEntity extends ChangeNotifier {
	String? name = "小明";
	List<PetEntity> pets = [
		PetEntity.init('小白',"猫"),PetEntity.init('小黑',"狗")
	];

	PeopleEntity();

	factory PeopleEntity.fromJson(Map<String, dynamic> json) => $PeopleEntityFromJson(json);


	Map<String, dynamic> toJson() => $PeopleEntityToJson(this);

	void firstPetRename(String newName){
		if(pets.isEmpty){
			return ;
		}
		if (newName == "") {
			return;
		}
		// PetEntity pet = PetEntity.init(newName, pets[0].type);
		// pets[0] = pet;
		pets[0].name = newName;
		notifyListeners();
	}

	void secondPetRename(String newName){
		if(pets.isEmpty){
			return ;
		}
		if (newName == "") {
			return;
		}
		// PetEntity pet = PetEntity.init(newName, pets[1].type);
		// pets[1] = pet;
		pets[1].name = newName;
		notifyListeners();
	}

	@override
	String toString() {
		return jsonEncode(this);
	}
}
